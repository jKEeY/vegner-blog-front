import VGrowler from '@/components/card/growler/growler.vue'
import { mapState } from 'vuex'

export default {
  components: {
    VGrowler
  },
  computed: {
    ...mapState({
      notifications: state => state.notification.notifications
    })
  }
}
