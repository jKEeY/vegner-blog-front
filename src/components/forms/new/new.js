import { mapState } from 'vuex'

export default {
  data () {
    return {
      form: {
        title: '',
        description: '',
        body: ''
      }
    }
  },
  computed: {
    ...mapState({
      accountId: state => state.account.id
    })
  },
  methods: {
    save () {
      this.$store.dispatch('posts/add', {title: this.form.title, description: this.form.description, body: this.form.body, id: this.accountId})
    },
    reset () {
      this.form.title = ''
      this.form.description = ''
      this.form.body = ''
    }
  }
}
