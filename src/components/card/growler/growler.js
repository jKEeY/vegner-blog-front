import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {faTimes} from '@fortawesome/free-solid-svg-icons'

export default {
  props: {
    notification: {
      type: Object,
      required: true
    }
  },
  components: {
    FontAwesomeIcon
  },
  data () {
    return {
      cross: faTimes
    }
  },
  methods: {
    remove () {
      this.$store.commit('notification/REMOVE', this.$props.notification.id)
    }
  },
  mounted () {
    setTimeout(() => this.remove(), 5000)
  }
}
