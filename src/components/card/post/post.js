import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {faThumbsUp, faThumbsDown, faComments, faTimes, faPlus} from '@fortawesome/free-solid-svg-icons'
import { mapState } from 'vuex'

export default {
  props: {
    post: {
      type: Object,
      required: true
    },
    index: {
      type: Number,
      required: true
    }
  },
  data () {
    return {
      like: faThumbsUp,
      dis: faThumbsDown,
      comments: faComments,
      times: faTimes,
      plus: faPlus,
      commentUser: '',
      isOpened: false,
      isOpenedComment: false
    }
  },
  components: {
    FontAwesomeIcon
  },
  computed: {
    ...mapState({
      username: state => state.account.username,
      accountId: state => state.account.id
    })
  },
  methods: {
    addLike (add, postId) {
      this.$store.dispatch('posts/addLikeOrDisLike', {add, i: this.$props.index, postId, authorId: this.accountId})
      console.log(postId)
    },
    addDislike (add, postId) {
      this.$store.dispatch('posts/addLikeOrDisLike', {add, i: this.$props.index, postId, authorId: this.accountId})
    },
    addComment (postId, authorId, index, username) {
      this.$store.dispatch('posts/addComment', {postId, authorId: this.accountId, message: this.commentUser, index, username})
      this.commentUser = ''
    },
    readMore (body) {
      if (this.isOpened) {
        return body
      } else {
        return body.slice(0, 200)
      }
    }
  }
}
