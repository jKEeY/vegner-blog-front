import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState({
      auth: state => state.auth.auth,
      username: state => state.account.username
    })
  },
  methods: {
    posts () {
      this.$router.push({name: 'posts'})
    },
    signin () {
      this.$router.push({name: 'auth.login'})
    },
    signup () {
      this.$router.push({name: 'auth.register'})
    },
    newPost () {
      this.$router.push({name: 'new'})
    },
    logout () {
      this.$store.dispatch('auth/logout')
    }
  }
}
