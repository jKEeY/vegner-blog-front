import Vue from 'vue'
import app from './app/app.vue'
import router from './plugins/vue-router'
import store from './store'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import BootstrapVue from 'bootstrap-vue'
import VueCookie from 'vue-cookie'

import '@/filters'

Vue.config.productionTip = false

const apolloProvider = new VueApollo({
  defaultClient: new ApolloClient({
    link: new HttpLink({
      uri: 'https://vegner.tk/api'
    }),
    cache: new InMemoryCache(),
    connectToDevTools: true
  })
})

Vue.use(VueApollo)
Vue.use(BootstrapVue)
Vue.use(VueCookie)

store.dispatch('auth/check')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  apolloProvider,
  render: h => h(app)
})
