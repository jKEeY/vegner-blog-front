export default {
  path: '/auth',
  name: 'auth',
  component: () => import('@/pages/auth/auth.vue'),
  children: [
    {
      path: 'login',
      name: 'auth.login',
      component: () => import('@/pages/auth/login/login.vue')
    },
    {
      path: 'register',
      name: 'auth.register',
      component: () => import('@/pages/auth/register/register.vue')
    }
  ],
  meta: {
    guest: true
  }
}
