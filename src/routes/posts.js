export default {
  path: '/',
  name: 'posts',
  component: () => import('@/pages/posts/posts.vue'),
  meta: {
    auth: true
  }
}
