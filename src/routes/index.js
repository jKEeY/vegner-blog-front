import posts from './posts'
import auth from './auth'
import newPost from './new'

export default [
  posts,
  auth,
  newPost
]
