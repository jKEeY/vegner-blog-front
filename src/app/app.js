import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VHeader from '@/components/header/header.vue'
import VGrowler from '@/components/growler/growler.vue'

export default {
  components: {
    VHeader,
    VGrowler
  }
}
