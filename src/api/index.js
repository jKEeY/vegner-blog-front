import axios from 'axios'
import Vue from 'vue'
import {print} from 'graphql'

export class Api {
  go (query, variables) {
    const token = Vue.cookie.get('token')
    return new Promise((resolve, reject) => {
      axios({
        url: 'http://vegner.tk/api',
        method: 'post',
        headers: {
          Authorization: `Bearer ${token}`
        },
        withCredentials: 'http://localhost:8080',
        data: {
          query: print(query),
          variables: variables || {}
        }
      })
        .then(data => {
          console.log(data)
          resolve(data.data)
        })
        .catch(error => {
          console.log(error)
          reject(error)
        })
    })
  }
}

export default new Api()
