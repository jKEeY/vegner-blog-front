import Vue from 'vue'
import Router from 'vue-router'
import routes from '@/routes'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  routes,
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.auth) && !store.state.auth.auth) {
    next({
      name: 'auth.login'
    })
  } else if (to.matched.some(m => m.meta.guest) && store.state.auth.auth) {
    next('/')
  } else {
    next()
  }
})

Vue.router = router

export default router
