import gql from 'graphql-tag'

export default Object.freeze({
  ADD: gql`mutation ($title:String, $body:String, $description: String, $author_id:String) {
    createPost (title: $title, body: $body, description: $description, author_id: $author_id) {
      success
    }
  }`,
  LOAD: gql` 
  query {
    posts {
      _id
      title
      description
      body
      created
      author {
        _id
        username
      }
      isOpened
      comments {
        author_id
        username
        message
        created
      }
      isLiked
      isDisLiked
      likes
      disliked
    }
  }

  `,
  ADDCOMMENT: gql`
  mutation ($post_id:String, $author_id: String, $message: String) {
    comment (post_id: $post_id, author_id: $author_id, message: $message) {
      success
    }
  }`,
  DELETEDISLIKE: gql`
    mutation ($post_id: String, $author_id: String) {
      deleteDisLike(post_id: $post_id, author_id: $author_id) {
        success
      }
    }
  `,
  ADDDISLIKE: gql`
    mutation ($post_id: String, $author_id: String) {
      addDislike(post_id: $post_id, author_id: $author_id) {
        success
      }
    }`,
  ADDLIKE: gql`
  mutation ($post_id: String, $author_id: String) {
    addLike(post_id: $post_id, author_id: $author_id) {
      success
    }
  }
`,
  DELETELIKE: gql`
  mutation ($post_id: String, $author_id: String) {
    deleteLike(post_id: $post_id, author_id: $author_id) {
      success
    }
  }`
})
