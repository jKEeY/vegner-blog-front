import Queries from './queries'
import Vue from 'vue'
import * as types from './mutations-types'
import store from '@/store'
import api from '@/api'

export const add = ({commit}, {title, description, body, id}) => {
  api.go(Queries.ADD, {
    title,
    body,
    description,
    author_id: id
  })
    .then(data => {
      if (data.data.createPost.success) {
        Vue.router.push({name: 'posts'})
      }
    })
}

export const load = ({commit}, payload) => {
  api.go(Queries.LOAD)
    .then(data => {
      commit(types.LOAD, data.data.posts.reverse())
      commit('CHANGELOAD', false)
    })
}

export const addComment = ({commit}, {postId, authorId, message, index, username}) => {
  api.go(Queries.ADDCOMMENT, {
    post_id: postId,
    author_id: authorId,
    message
  })
    .then(data => {
      if (data.data.comment.success) {
        commit(types.COMMENTADD, {username, message, i: index})
      }
    })
}

export const addLikeOrDisLike = ({commit, dispatch}, {add, i, postId, authorId}) => {
  if (add === 0) {
    if (!store.state.posts.arrPosts[i].isLiked && store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.DELETEDISLIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.deleteDisLike.success) {
            api.go(Queries.ADDLIKE, {
              post_id: postId,
              author_id: authorId
            })
              .then(data => {
                if (data.data.addLike.success) {
                  commit(types.ADDLIKEORDISLIKE, {pay: 'DELETEDISLIKE', i})
                  commit(types.ADDLIKEORDISLIKE, {pay: 'ADDLIKE', i})
                  // dispatch('addMessage', {message: 'Лайк поставлен!'})
                  store.commit('notification/ADD', {message: 'Лайк поставлен!'})
                }
              })
          }
        })
    } else if (!store.state.posts.arrPosts[i].isLiked && !store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.ADDLIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.addLike.success) {
            commit(types.ADDLIKEORDISLIKE, {pay: 'ADDLIKE', i})
            store.commit('notification/ADD', {message: 'Лайк поставлен!'})
          }
        })
    } else if (store.state.posts.arrPosts[i].isLiked && !store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.DELETELIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.deleteLike.success) {
            commit(types.ADDLIKEORDISLIKE, {pay: 'DELETELIKE', i})
            store.commit('notification/ADD', {message: 'Лайк удалён!'})
          }
        })
    }
  }
  if (add === 1) {
    if (store.state.posts.arrPosts[i].isLiked && !store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.DELETELIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.deleteLike.success) {
            api.go(Queries.ADDDISLIKE, {
              post_id: postId,
              author_id: authorId
            })
              .then(data => {
                if (data.data.addDislike.success) {
                  commit(types.ADDLIKEORDISLIKE, {pay: 'DELETELIKE', i})
                  commit(types.ADDLIKEORDISLIKE, {pay: 'ADDDISLIKE', i})
                  store.commit('notification/ADD', {message: 'ДизЛайк поставлен!'})
                }
              })
          }
        })
    } else if (!store.state.posts.arrPosts[i].isLiked && !store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.ADDDISLIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.addDislike.success) {
            commit(types.ADDLIKEORDISLIKE, {pay: 'ADDDISLIKE', i})
            store.commit('notification/ADD', {message: 'ДизЛайк поставлен!'})
          }
        })
    } else if (!store.state.posts.arrPosts[i].isLiked && store.state.posts.arrPosts[i].isDisLiked) {
      api.go(Queries.DELETEDISLIKE, {
        post_id: postId,
        author_id: authorId
      })
        .then(data => {
          if (data.data.deleteDisLike.success) {
            commit(types.ADDLIKEORDISLIKE, {pay: 'DELETEDISLIKE', i})
            store.commit('notification/ADD', {message: 'ДизЛайк удалён!'})
          }
        })
    }
  }
}

export default {
  add,
  load,
  open,
  addComment,
  addLikeOrDisLike
}
