import * as types from './mutations-types'

export default {
  [types.LOAD] (state, payload) {
    state.arrPosts = payload
  },
  [types.CHANGELOAD] (state, payload) {
    state.isLoad = payload
  },
  [types.COMMENTADD] (state, {username, message, i}) {
    console.log(state.arrPosts)
    state.arrPosts[i].comments.push({username, message, created: Date.now()})
  },
  [types.ADDLIKEORDISLIKE] (state, payload) {
    switch (payload.pay) {
      case 'ADDLIKE':
        state.arrPosts[payload.i].isLiked = true
        state.arrPosts[payload.i].likes += 1
        break
      case 'DELETELIKE':
        state.arrPosts[payload.i].isLiked = false
        state.arrPosts[payload.i].likes -= 1
        break
      case 'ADDDISLIKE':
        state.arrPosts[payload.i].isDisLiked = true
        state.arrPosts[payload.i].disliked += 1
        break
      case 'DELETEDISLIKE':
        state.arrPosts[payload.i].isDisLiked = false
        state.arrPosts[payload.i].disliked -= 1
        break
    }
  }
}
