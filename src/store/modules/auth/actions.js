import Qieries from './queries'
import * as types from './mutations-types'
import Vue from 'vue'
import api from '@/api'

const login = ({ commit }, {username, password}) => {
  api.go(Qieries.LOGIN, {username, password})
    .then(data => {
      if (data.data.login.success) {
        commit(types.LOGIN)
        Vue.router.push('/')
      }
    })
}

const register = ({ commit }, payload) => {
  api.go(Qieries.REGISTER, {username: payload.username, password: payload.password})
    .then(data => {
      if (data.data.signup.success) {
        commit(types.LOGIN)
        Vue.router.push('/')
      }
    })
}

const check = ({commit}) => {
  api.go(Qieries.CHECK)
    .then(data => {
      if (data.data.isLoggingIn) {
        commit(types.LOGIN)
        Vue.router.push('/')
      }
    })
}

const logout = ({commit}) => {
  Vue.cookie.delete('token')
  commit(types.LOGOUT)
  Vue.router.push({name: 'auth.login'})
}

export default {
  login,
  register,
  check,
  logout
}
