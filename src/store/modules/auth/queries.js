import gql from 'graphql-tag'

export default Object.freeze({
  LOGIN: gql`
    query ($username:String, $password:String) {
      login(username: $username, password: $password) {
        success
        message
      }
    }
  `,
  REGISTER: gql`
  mutation ($username: String, $password: String) {
    signup(username: $username, password: $password) {
      success
      message
    }
  }
  `,
  CHECK: gql`
  query {
    isLoggingIn
  }
  `
})
