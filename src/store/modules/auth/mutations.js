import * as types from './mutations-types'

export default {
  [types.LOGIN] (state) {
    state.auth = true
    state.error = null
    state.failed = false
  },
  [types.LOGOUT] (state) {
    state.auth = false
    state.error = null
    state.failed = false
  }
}
