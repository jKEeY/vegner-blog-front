import gql from 'graphql-tag'

export default Object.freeze({
  PROFIL: gql`
      query {
        profile {
          _id
          username
        }
      }
      
      `
})
