import * as types from './mutations-types'

export default {
  [types.FIND] (state, payload) {
    state.id = payload._id
    state.username = payload.username
  },
  [types.CLEAR] (state) {
    state.id = null
    state.username = null
  }
}
