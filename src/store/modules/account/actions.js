import * as types from './mutations-types'
import Queries from './queries'
import api from '@/api'

const find = ({ commit }) => {
  api.go(Queries.PROFIL)
    .then(data => {
      commit(types.FIND, data.data.profile)
    })
}

export default {
  find
}
