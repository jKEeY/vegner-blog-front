export const FIND = 'FIND'
export const CLEAR = 'CLEAR'

export default {
  FIND,
  CLEAR
}
