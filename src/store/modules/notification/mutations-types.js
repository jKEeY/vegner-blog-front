export const ADD = 'ADD'
export const REMOVE = 'REMOVE'

export default {
  ADD,
  REMOVE
}
