import * as types from './mutations-types'

export const addMessage = ({commit}, payload) => {
  commit(types.ADD, payload)
}

export const removeMessage = ({commit}, payload) => {
  commit(types.ADD, payload)
}

export default {
  addMessage,
  removeMessage
}
