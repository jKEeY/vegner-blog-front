import * as types from './mutations-types'

export default {
  [types.ADD] (state, payload) {
    payload.id = Math.random() * (1000 - 100000000000) + 100000000000
    state.notifications.push(payload)
  },
  [types.REMOVE] (state, payload) {
    state.notifications = state.notifications.filter(el => el.id !== payload)
  }
}
