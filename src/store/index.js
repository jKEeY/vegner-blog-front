import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

// Modules
import account from './modules/account'
import auth from './modules/auth'
import posts from './modules/posts'
import notification from './modules/notification'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    account,
    auth,
    posts,
    notification
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
