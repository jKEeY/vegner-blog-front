import Vue from 'vue'
import formatDate from './formatDate'
import timeAgo from './timeAgo'

Vue.filter('formatDate', formatDate)
Vue.filter('timeAgo', timeAgo)
