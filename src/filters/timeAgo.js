import distanceInWordsToNow from 'date-fns/distance_in_words_to_now'
import ru from 'date-fns/locale/ru'

export default function timeAgo (date) {
  return distanceInWordsToNow(date, { locale: ru, addSuffix: true })
}
