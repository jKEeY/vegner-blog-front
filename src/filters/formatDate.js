import format from 'date-fns/format'
import ru from 'date-fns/locale/ru'

export default function formatDate (date) {
  return format(
    new Date(date),
    'D MMMM YYYY в HH:mm',
    { locale: ru }
  )
}
