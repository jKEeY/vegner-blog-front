import VCardPost from '@/components/card/post/post.vue'
import { mapState } from 'vuex'

export default {
  components: {
    VCardPost
  },
  computed: {
    ...mapState({
      posts: state => state.posts.arrPosts,
      isLoading: state => state.posts.isLoad
    })
  },
  beforeCreate () {
    this.$store.dispatch('account/find')
    this.$store.dispatch('posts/load')
  }
}
