import VFormNew from '@/components/forms/new/new.vue'

export default {
  components: {
    VFormNew
  },
  created () {
    this.$store.dispatch('account/find')
  }
}
