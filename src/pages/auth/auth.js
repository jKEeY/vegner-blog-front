export default {
  methods: {
    login () {
      this.$router.push({name: 'auth.login'})
    },
    reg () {
      this.$router.push({name: 'auth.register'})
    }
  }
}
