export default {
  data () {
    return {
      form: {
        username: null,
        password: null
      }
    }
  },
  methods: {
    reset () {
      this.form.username = null
      this.form.password = null
    },
    authLogin (e) {
      e.preventDefault()
      this.$store.dispatch('auth/login', {username: this.form.username, password: this.form.password})
    }
  }
}
