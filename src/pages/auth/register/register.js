export default {
  data () {
    return {
      form: {
        email: null,
        username: null,
        password: null
      }
    }
  },
  methods: {
    reset () {
      this.form.username = null
      this.form.password = null
    },
    register (e) {
      e.preventDefault()
      this.$store.dispatch('auth/register', {username: this.form.username, password: this.form.password})
    }
  }
}
